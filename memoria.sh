#!/bin/bash

# Script para comprobar el uso de memoria del equipo.
# Cada 5 minutos, cron/systemd lanzará el comando
# "$(( $(cat /proc/meminfo | grep MemAvailable | awk '{ print $2 }') / 1024 )) >> /home/anto/Scripts/Rendimiento/memoria.txt".
# De ese archivo, se tomará el valor entero de la columna idle y se usará para hacer la media de uso.
# Se almacenarán el dato más alto, el más bajo y la media.
# Los datos se guardan en un fichero para ser enviado por correo mensualmente.
# V2 28/Julio/2018
# Antonio Arroba González

# Definición de variables

total=$(( $(cat /proc/meminfo | grep MemTotal | awk '{ print $2 }') / 1024 ))
suma_ram=0
conteo_ram=0
max_ram=$total
min_ram=0
valores=$(cat /home/anto/Scripts/Rendimiento/memoria.txt)

# Uso de RAM

# Se actualizan los valores máximos y mínimos

for valor in $valores

do

	# Actualizamos el valor más alto registrado

	if [ $valor -lt $max_ram ]
	then
		max_ram=$valor
	fi

	# Actualizamos el valor mínimo

	if [ $valor -gt $min_ram ]
	then
		min_ram=$valor
	fi

done

# Se calcula la media de idle. A la hora de mostrar el valor real de uso,
# restaremos lo calculado a 100.


for valor in $valores

do

	suma_ram=$(( $suma_ram + $valor ))

	conteo_ram=$(( $conteo_ram +1 ))

done

echo Media de RAM: $(( $total - $suma_ram / $conteo_ram ))
echo Máximo de RAM: $(( $total - $max_ram ))
echo Mínimo de RAM: $(( $total - $min_ram ))

#printf "%s %s" "$(( $(cat /proc/meminfo | grep MemAvailable | awk '{ print $2 }') / 1024 ))" "MB"
