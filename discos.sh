#!/bin/bash

# Script para comprobar el estado de los discos y el uso de las particiones.
# Los datos se guardan en un fichero para ser enviado por correo mensualmente.
# V1 10/Junio/2018
# Antonio Arroba González

# Definición de variables

# Esta es la forma de definir un array en Shell Scripting, para recorrerla en un for usamos "${variable[@]}"
discos=( /dev/sda /dev/sdb /dev/sdc )
particiones=( /dev/sda[2-3] /dev/sda[5-7] /dev/sdc[1-2] )


# Definición de las tareas de chequeo

# Indicamos lo primero fecha y hora de ejecución

echo $(date)
echo ---------------------- $'\n'


# Chequeo del estado S.M.A.R.T. de los discos

for disco in "${discos[@]}"

	do

		echo Estado de disco $disco
	

		echo $'\n'
	

		smartctl -H $disco | grep PASSED &>/dev/null && echo OK || echo Disco con errores, revisar
	

		echo ······················ $'\n'
	

	done

echo ---------------------- $'\n'

# Uso de disco

#for i in $(fdisk -l | grep sd[a-c][1-3] | awk '{ print $1 }')
for particion in "${particiones[@]}"

	do

		echo Estado de la partición $particion
	
		echo Punto de montaje: $(df -h | grep $particion | awk '{ print $6 }')
	
		echo Tamaño: $(df -h | grep $particion | awk '{ print $2 }')
	
		echo Usado: $(df -h | grep $particion | awk '{ print $3 }')
	
		echo Disponible: $(df -h | grep $particion | awk '{ print $4 }')
	
		echo Porcentaje usado: $(df -h | grep $particion | awk '{ print $5 }')
	

		echo ······················ $'\n'
	

	done

echo ---------------------- $'\n'

