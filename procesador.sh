#!/bin/bash

# Script para comprobar el uso de procesador y su media.
# Cada 5 min_cpuutos, cron/systemd lanzará el comando
# "sar -u 2 10| awk '{ printf "%-10s %-10s\n", $1,$8 }' >> /home/anto/Scripts/Rendimiento/cpu.txt".
# De ese archivo, se tomará el valor entero de la columna idle y se usará para hacer la media de uso.
# Se almacenarán el dato más alto, el más bajo y la media.
# Los datos se guardan en un fichero para ser enviado por correo mensualmente.
# V1 28/Julio/2018
# Antonio Arroba González

# Definición de variables

suma_cpu=0
conteo_cpu=0
max_cpu=100
min_cpu=0
valores=$(grep Media /home/anto/Scripts/Rendimiento/cpu.txt | awk '{ printf "%i\n", $2 }')

# Uso de procesador

# Se actualizan los valores máximos y mínimos

for valor in $valores

do

	# Actualizamos el valor más alto registrado

	if [ $valor -lt $max_cpu ]
	then
		max_cpu=$valor
	fi

	# Actualizamos el valor mínimo

	if [ $valor -gt $min_cpu ]
	then
		min_cpu=$valor
	fi

done

# Se calcula la media de idle. A la hora de mostrar el valor real de uso,
# restaremos lo calculado a 100.


for valor in $valores

do

	suma_cpu=$(( $suma_cpu + $valor ))

	conteo_cpu=$(( $conteo_cpu +1 ))

done

echo Media de cpu: $(( 100 - $suma_cpu / $conteo_cpu ))
echo Máximo de cpu: $(( 100 - $max_cpu ))
echo Mínimo de cpu: $(( 100 - $min_cpu ))

