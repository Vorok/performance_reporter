#!/bin/bash

# Script para enviar correo mensual del estado del equipo.
# El script leerá los datos de los archivos de memoria y cpu y ejecutará unas tareas sobre los discos.
# A continuación lo mandará por e-mail.
# V2 31/Diciembre/2018: Corrección de errores
# V1 28/Julio/2018
# Antonio Arroba González

# Declaración de funciones.

function procesador {

# Variables

suma_cpu=0
contador_cpu=0
max_cpu=100
min_cpu=0
valores_cpu=$(grep Media /home/anto/Scripts/Rendimiento/cpu.txt | awk '{ printf "%i\n", $2 }')

# Uso de procesador

# Se actualizan los valores máximos y mínimos

for valor_cpu in $valores_cpu

do

	# Actualizamos el valor más alto registrado

	if [ $valor_cpu -lt $max_cpu ]
	then
		max_cpu=$valor_cpu
	fi

	# Actualizamos el valor mínimo

	if [ $valor_cpu -gt $min_cpu ]
	then
		min_cpu=$valor_cpu
	fi

done

# Se calcula la media de idle. A la hora de mostrar el valor real de uso,
# restaremos lo calculado a 100.


for valor_cpu in $valores_cpu

do

	suma_cpu=$(( $suma_cpu + $valor_cpu ))

	contador_cpu=$(( $contador_cpu +1 ))

done

echo Media de CPU utilizada: $(( 100 - $suma_cpu / $contador_cpu ))
echo Máximo de CPU utilizada: $(( 100 - $max_cpu ))
echo Mínimo de CPU utilizada: $(( 100 - $min_cpu ))

> /home/anto/Scripts/Rendimiento/cpu.txt

}

function memoria {

# Variables

total_ram=$(( $(cat /proc/meminfo | grep MemTotal | awk '{ print $2 }') / 1024 ))
suma_ram=0
contador_ram=0
max_ram=$total_ram
min_ram=0
valores_ram=$(cat /home/anto/Scripts/Rendimiento/memoria.txt)

# Uso de RAM

# Se actualizan los valores máximos y mínimos

for valor_ram in $valores_ram

do

	# Actualizamos el valor más alto registrado

	if [ $valor_ram -lt $max_ram ]
	then
		max_ram=$valor_ram
	fi

	# Actualizamos el valor mínimo

	if [ $valor_ram -gt $min_ram ]
	then
		min_ram=$valor_ram
	fi

done

# Se calcula la media de idle. A la hora de mostrar el valor real de uso,
# restaremos lo calculado a 100.

for valor_ram in $valores_ram

do

	suma_ram=$(( $suma_ram + $valor_ram ))

	contador_ram=$(( $contador_ram +1 ))

done

echo Media de RAM utilizada: $(( $total_ram - $suma_ram / $contador_ram ))
echo Máximo de RAM utilizada: $(( $total_ram - $max_ram ))
echo Mínimo de RAM utilizada: $(( $total_ram - $min_ram ))

> /home/anto/Scripts/Rendimiento/memoria.txt

}

function discos {

# Variables

# Esta es la forma de definir un array en Shell Scripting, para recorrerla en un for usamos "${variable[@]}"
discos=( /dev/sda /dev/sdb /dev/sdc )
particiones=( /dev/sda[2-3] /dev/sda[5-7] /dev/sdb1 /dev/sdc1 )

# Chequeo del estado S.M.A.R.T. de los discos

for disco in "${discos[@]}"

	do

		echo Estado de disco $disco
	

		echo
	

		/sbin/smartctl -H $disco | grep PASSED &>/dev/null && echo OK || echo Disco con errores, revisar
	

		echo ······················
	

	done

echo ----------------------

# Uso de disco

for particion in "${particiones[@]}"

	do

		echo Estado de la partición $particion
	
		echo Punto de montaje: $(df -h | grep $particion | awk '{ print $6 }')
	
		echo Tamaño: $(df -h | grep $particion | awk '{ print $2 }')
	
		echo Usado: $(df -h | grep $particion | awk '{ print $3 }')
	
		echo Disponible: $(df -h | grep $particion | awk '{ print $4 }')
	
		echo Porcentaje usado: $(df -h | grep $particion | awk '{ print $5 }')
	

		echo ······················
	

	done

}

# Escritura y envío del mail.

# Escritura y envío del mail.

echo "

$(date)

Inicio del informe:

######################

$(procesador)

----------------------

$(memoria)

----------------------

$(discos)

######################

Fin del informe.


" | mail -s "Informe del estado del NAS día $(date +%d/%m/%Y)" sdn.heavy@gmail.com